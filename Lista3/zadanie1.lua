
utf8.reverse = function (s)
    codes = {}
    for i, c in utf8.codes(s) do
      codes[#codes+1] = c
    end
    
    ans = ''
    for i=#codes, 1, -1  do
      ans = ans .. utf8.char(codes[i])
    end    
    return ans
end
print(utf8.reverse('Księżyc'))