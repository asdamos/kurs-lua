

table.contains = function(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

utf8.reverse = function (s)
    codes = {}
    for i, c in utf8.codes(s) do
      codes[#codes+1] = c
    end
    
    ans = ''
    for i=#codes, 1, -1  do
      ans = ans .. utf8.char(codes[i])
    end    
    return ans
end

string.strip = function(s, w)
    local whitespace_codes = {9,10,11,12,13,32}
    
    ans = ''
    
    local s_codes = {}
      for i, c in utf8.codes(s) do
        s_codes[#s_codes + 1] = c
    end
    local iter = #s_codes

    if w == nil then
      while iter > 0 and table.contains(whitespace_codes, s_codes[iter]) do
        iter = iter - 1
      end
    
      for i=1, iter do
        ans = ans .. utf8.char(s_codes[i])
      end
      
    
    else
      local w_codes = {}
      for i, c in utf8.codes(w) do
        w_codes[#w_codes + 1] = c
      end
      
      while iter > 0 and table.contains(w_codes, s_codes[iter]) do
        iter = iter - 1
      end
    
      for i=1, iter do
        ans = ans .. utf8.char(s_codes[i])
      end
      
    end
    
    return ans

end

print(string.strip(' test string ') .. '|')
print(string.strip(' test string', 'tng') .. '|')

