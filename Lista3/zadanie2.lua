
utf8.normalize = function(s)
  ans = ''
  for i, c in utf8.codes(s) do
      if c <= 127 then
        ans = ans .. utf8.char(c)
      end
  end
  return ans
end

print(utf8.normalize('Księżyc:\nNów'))