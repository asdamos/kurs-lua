
utf8.sub = function(s, i, j)
  local end_index = 0
  if j == nil or j == -1 then
    end_index = #s
  else
    end_index = j
  end
  ans = ''
  
  codes = {}
  for index, c in utf8.codes(s) do
      codes[#codes + 1] = c
  end
  
  for iter=1, #codes do
    if  i <= iter and iter <= end_index then
      ans = ans .. utf8.char(codes[iter])
    end
  end
  
  return ans
end


print(utf8.sub('Księżyc:\nNów',5,10))