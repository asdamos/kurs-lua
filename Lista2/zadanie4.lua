calendar = {}


function addEvent(day, startTime, endTime, name)
  local events = calendar[day]
  
  local eventOk = true
  local conflict = ""
  
  --There is some event on that day
  if(type(events) == "table") then
    for i,e in ipairs(events) do
        if (e.startTime < startTime and startTime < e.endTime) or (e.startTime < endTime and endTime < e.endTime) then
          eventOk = false
          conflict = e.name
        end
    end
    
    if eventOk then
      calendar[day][#events + 1] = {startTime = startTime, endTime = endTime, name = name}
      
      return true
    else
      return false, "Conflict with event named: " .. conflict
    end      
  end
  
  --There are no events on that day
  calendar[day] = {}
  calendar[day][1] = {startTime = startTime, endTime = endTime, name = name}
  return true
end

function addEventAndPrintInfo(day, startTime, endTime, name)
    local result, conflict = addEvent(day, startTime, endTime, name)
    
    if result then
      print("Succes")
    else
      print("Failure")
      print(conflict)
    end
end

addEventAndPrintInfo("wtorek", 16, 18, "Wykład Lua")

addEventAndPrintInfo("wtorek", 15, 17, "Wykład Java")

addEventAndPrintInfo("wtorek", 18, 20, "Ćwiczenia Lua")

addEventAndPrintInfo("środa", 10, 20, "Wykład AiSD")

addEventAndPrintInfo("13.03.2017", 15, 18, "Spotkanie 1")

addEventAndPrintInfo("13.03.2017", 16, 17, "Spotkanie 2")
