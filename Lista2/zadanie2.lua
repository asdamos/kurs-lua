function printtab(tab)
  str = ''
  for i=1,#tab do
    str = str..tab[i]..' '
  end
  print (str)
end

function isSequence(...)
  local tab = ...
  local indexes = {}
  local borders = {}
  
  if type(tab) == "table" then
    for i, v in pairs(tab) do
      if type(i) == "number" then
        indexes[#indexes + 1] = i
      end
    end

  else
    for i,v in pairs{...} do
      if type(i) == "number" then
        indexes[#indexes + 1] = i
      end
    end
    
  end

  table.sort(indexes)

  for i=1, #indexes do
    
    if indexes[i] + 1 ~= indexes[i+1] then
      borders[#borders + 1] = indexes[i]
    end
    
  end
  
  local result = true
  if #borders == 0 then
    borders[1] = 0
  end
  
  if #borders > 1 then
    result = false
  end
  
  if indexes[1] ~= 1 then
    result = false
  end
  
  
  return result, borders
end



tab = {nil,1,2,3}

local res, borders = isSequence(tab)

print(res)
printtab(borders)


t={1, 2, 3, nil, 'pięć', 'sześć'}
t[1000]='xyz'

res, borders = isSequence(t)
print(res)
printtab(borders)


res, borders = isSequence(1,2,nil,4,nil,"sześć")
print(res)
printtab(borders)

res, borders = isSequence(1,2,3)
print(res)
printtab(borders)


