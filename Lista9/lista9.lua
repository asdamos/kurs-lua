local zad9 = require'biblioteka'

function printTable(t)
  res = ""
  for _, v in ipairs( t ) do
   res = res .. v .. " "
  end
  print(res)
end

function printTable2(t)
  print("Key", "Value")
  for k, v in pairs( t ) do
   print(k,v)
  end
end


print("Summation")
print("summation(): " .. zad9.summation())
print("summation(1,2,3,4,5): " .. zad9.summation(1,2,3,4,5))


function plus(x, y)
  return x+y
end
print("\nReduce")
print("reduce(plus, {1,2,3,4})): " .. zad9.reduce(plus, {1,2,3,4}))
print("reduce(plus, {1,2,3,4}, 5)): " .. zad9.reduce(plus, {1,2,3,4}, 5))


function even(x)
  if x%2 == 0 then
    return true
  else
    return false
  end
end
print("\nFilter")
print("filter(even, {1,2,3,4})):")
printTable(zad9.filter(even, {1,2,3,4}))


print("\nReverse")
print("reverse({1,2,3,4}):")
printTable(zad9.reverse({1,2,3,4}))
print("reverse({'ala', 'ma', 'kota'}):")
printTable(zad9.reverse({"ala", "ma", "kota"}))


print("\nMerge")

t = {}
t[1] = "abc"

t1 = {}
t1["b"] = 1
t1["c"] = 3

t2 = {}
t2["a"] = 5
t2["aaaa"] = "bbbb"

t3 = {}
t3["a"] = 100
t3[5] = 100

t4 = {}

printTable2(zad9.merge(t,t1,t2,t3,t4))





