#include <math.h>
#include <lua.hpp>
#include <iostream>

static void stackDump (lua_State *L)
{
    std::cout<<"StackDump:"<<std::endl;
    int i;
    int top = lua_gettop(L);
    for (i = top; i > 0; i--) // odwiedzamy elementy od gory
    {
        std::cout<<"Indeks "<<i<<": ";
        int t = lua_type(L, i);
        switch (t)
        {
        case LUA_TSTRING: // napis
            std::cout<<"'"<<lua_tostring(L, i)<<"'"<<std::endl;
            break;

        case LUA_TBOOLEAN: // wartość logiczna
            if (lua_toboolean(L, i))
            {
                std::cout<<"true"<<std::endl;
            }
            else
            {
                std::cout<<"false"<<std::endl;
            }
            break;

        case LUA_TNUMBER: // liczby

            if (lua_isinteger(L, i)) // integer
            {
                std::cout<<lua_tointeger(L, i)<<std::endl;
            }
            else // float
            {
                std::cout<<lua_tonumber(L, i)<<std::endl;
            }
            break;

        default: // pozostałe
            std::cout<<lua_typename(L, t)<<std::endl;
            break;
        }
    }
    std::cout<<std::endl;
}

static int l_summation(lua_State *L)
{
    double res = 0;

    int n = lua_gettop(L);
    for(int i=1; i<=n; i++)
    {
        res += luaL_checknumber(L, i);
    }

    lua_pushnumber(L, res);
    return 1;
}

static int l_reduce(lua_State *L)
{
    if(lua_gettop(L) != 2 && lua_gettop(L) != 3)
    {
        return luaL_error(L, "function takes 2 or 3 parameters");
    }

    if(!lua_isfunction(L, 1))
    {
        return luaL_error(L, "parameter 1 is not a function");
    }

    if(!lua_istable(L, 2))
    {
        return luaL_error(L, "parameter 2 is not a table");
    }

    lua_len(L, 2);
    int n = lua_tointeger(L, -1);
    lua_pop(L, 1);

    int i=1;

    if(lua_gettop(L) == 2)
    {
        if(n < 2)
        {
            return luaL_error(L, "table is too small");
        }

        lua_pushinteger(L, 1);  //get first element from table
        lua_gettable(L, 2);

        i=2;

    }
    else    //gettop == 3
    {
        if(n < 1)
        {
            return luaL_error(L, "table is too small");
        }
    }

    for(; i<=n; i++)
    {
        lua_pushinteger(L, i);
        lua_gettable(L, 2);

        //on stack: function, table, element, element

        lua_pushnil(L);
        //on stack: function, table, element, element, nil

        lua_copy(L, 1, 5);
        //on stack: function, table, element, element, function
        lua_insert(L, -3);

        if(lua_pcall(L, 2, 1, 0) != LUA_OK)
        {
            return luaL_error(L, "problem with running function");
        }
    }
    return 1;
}

static int l_filter(lua_State *L)
{
    if(lua_gettop(L) != 2)
    {
        return luaL_error(L, "function takes 2 parameters");
    }

    if(!lua_isfunction(L, 1))
    {
        return luaL_error(L, "parameter 1 is not a function");
    }

    if(!lua_istable(L, 2))
    {
        return luaL_error(L, "parameter 2 is not a table");
    }

    lua_len(L, 2);
    int n = lua_tointeger(L, -1);
    lua_pop(L, 1);

    lua_newtable(L);
    //on stack: function, table-in, table-out

    int j=1;


    for(int i=1; i<=n; i++)
    {
        lua_pushinteger(L, i);
        lua_gettable(L, 2);

        lua_pushnil(L);
        lua_copy(L, 1, 5);
        //on stack: function, table-in, table-out, element, function
        lua_insert(L, -2);

        if(lua_pcall(L, 1, 1, 0) != LUA_OK)
        {
            return luaL_error(L, "problem with running function");
        }

        //on stack: function, table-in, table-out, result

        if(!lua_isboolean(L, -1))
        {
            return luaL_error(L, "function did not return boolean");
        }

        bool res = lua_toboolean(L, -1);

        lua_pop(L, 1);

        if(res)
        {
            lua_pushinteger(L, j);  //index in result table

            //get value
            lua_pushinteger(L, i);
            lua_gettable(L, 2);

            //on stack: function, table-in, table-out, index, value
            lua_settable(L, 3);
            j++;
        }
    }
    return 1;
}

static int l_reverse(lua_State *L)
{
    if(lua_gettop(L) != 1)
    {
        return luaL_error(L, "function takes one parameter");
    }

    if(!lua_istable(L, 1))
    {
        return luaL_error(L, "parameter is not a table");
    }

    lua_len(L, 1);
    int n = lua_tointeger(L, -1);
    lua_pop(L, 1);

    for(int i=1; i<=n/2; i++)
    {
        lua_pushinteger(L, i);
        lua_gettable(L, -2);

        lua_pushinteger(L, n-i+1);
        lua_gettable(L, -3);

        //on stack: table, element from left, element from right

        lua_pushinteger(L, i);
        lua_insert(L, -2);
        lua_settable(L, 1);


        lua_pushinteger(L, n-i+1);
        lua_insert(L, -2);
        lua_settable(L, 1);
    }

    return 1;
}


static int l_merge(lua_State *L)
{
    int number_of_parameters = lua_gettop(L);

    for(int i=1; i<=number_of_parameters; i++)
    {
        if(!lua_istable(L, i))
        {
            return luaL_error(L, "parameter is not a table");
        }
    }

    for(int i=0; i<number_of_parameters-1; i++)
    {
        // table is in the stack at index 2
        lua_pushnil(L);  /* first key */
        while (lua_next(L, 2) != 0)
        {
            // ... key, value
            lua_pushnil(L);
            // ... key, value, nil

            lua_copy(L, -3, -1);

            // ... key, value, key

            if(lua_gettable(L, 1) == LUA_TNIL)
            {
                // ... key, value in ti, nil

                lua_copy(L, -2, -1);

                // ... key, value, value

                lua_copy(L, -3, -2);

                // ... key, key, value

                lua_settable(L, 1);

                // ... key
            }
            else
            {
                // ... key, value in ti, value in t

                lua_pop(L, 2);
                // ... key
            }
        }
        lua_remove(L, 2);   //remove this table
    }
    return 1;
}


static const struct luaL_Reg mylib [] = 
{
{"summation", l_summation},
{"reduce", l_reduce},
{"filter", l_filter},
{"reverse", l_reverse},
{"merge", l_merge},
{NULL, NULL}  // sentinel
};

extern "C" int luaopen_biblioteka(lua_State *L)  // wystawienie na zewnątrz (z extern C)
{
    luaL_newlib(L, mylib);
    return 1;
}
