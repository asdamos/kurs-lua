function isCorrectValuation(s)
  if type(s) ~= "string" then
    return
  end
  
  local x,y

  for word in string.gmatch(s, "%b()") do
    local sub = string.sub(word, 2, string.len(word)-1)  
    
    local nawiasowanieOk = isCorrectValuation(sub)
    if nawiasowanieOk == false then
      return false;
    end
  end
  
  s = string.gsub(s, "%b()", "0")
  
  x, y = string.match(s, "(.+)[+%-%*/](.+)")
  
  if type(x) ~= "nil" and type(y) ~= "nil" then
    local xbool = isCorrectValuation(x)
    local ybool = isCorrectValuation(y)
    return xbool and ybool
  end

  x=string.match(s, "%s*%-?%s*%d+%.?%d*%s*")
  
  if type(x) ~= "nil" and x == s then
    return true
  end

  return false
end


function test(s, expected)
  local val = isCorrectValuation(s)
   if val ~= expected then
    print("ALARM")
  end
  print(s .. "\t\t -> " .. tostring(val) .. " : " .. tostring(expected) .. "\n")
end

print("Test, wynik funkcji, wartość oczekiwana\n")

test("a     +     b", false)
test("test", false)
test("2+2", true)
test("1+2+3", true)
test("2+(2*2)", true)
test("2 + (2 * 2 )", true)
test("(2*(3-1*(4-3))/5)+(2-4)-(43*34)", true)
test("2+++", false)
test("2++2", false)
test("2+ -2", false)
test("2+(-2)", true)
test("*2+(-2)", false)
test("-2 + (-2)", true)
test("5+7!+9", false)
test("(22.22+22.2)+22.22)",false)
test("((22.22+22.2)+22.22)",true)
test("(((1)))", true)
test("((1)", false)
test("(1))", false)
test("-2 + 4.503", true)
test("(2*3.5*4)-(-12)/3", true)
test("1+(+2+)+3", false)
test("1+(+2)+3", false)
test("(1))", false)
test("((1)", false)
test("2 3", false)
test("-+-1", true)