kalendarz = {}

function createEventOsTime(name, startTime, endTime)
    event = {name=name, startTime=startTime, endTime=endTime}
    return event
end

function addEvent(calendar, event)
  
  if type(calendar) ~= "table" then
    return
  end
    
  for i, e in pairs(calendar) do
        if (e.startTime < event.startTime and event.startTime < e.endTime) or (e.startTime < event.endTime and event.endTime < e.endTime) then
          return nil, e
        end
    end
    
  calendar[event.startTime] = event
  
  return calendar[event.startTime]
end

function addEventAndPrintInfo(calendar, event)
  local x, y = addEvent(calendar, event)
  
  if type(x) == "nil" then
    print("Próba dodania wydarzenia: " .. event.name)
    print("Kolizja z wydarzeniem: " .. y.name)
    print("Początek wydarzenia: " .. os.date("%x, %X", y.startTime)) 
    print("Koniec wydarzenia: " .. os.date("%x, %X", y.startTime) .. "\n")
  else
    print("Pomyślnie dodano wydarzenie: " .. x.name)
    print("Początek wydarzenia: " .. os.date("%x, %X", x.startTime)) 
    print("Koniec wydarzenia: " .. os.date("%x, %X", x.startTime)  .. "\n")
  end
  
end

function show(calendar, all)
  local printAll = false
  
  if type(calendar) ~= "table" then
    return
  end
  
  if type(all) == "boolean" then
    if all then
      printAll = true
    end
  end
  
  local localCalendar = {}
  
  if printAll then
    for i, e in pairs(calendar) do
        localCalendar[#localCalendar + 1] = e
    end
    
    if #localCalendar == 0 then
      print("Kalendarz jest pusty")
    else
      print("Wszystkie wydarzenia z kalendarza:")
    end
    
  else
    local now = os.time()
    for i, e in pairs(calendar) do
      if e.startTime >= now then
        localCalendar[#localCalendar + 1] = e
      end
    end
    if #localCalendar == 0 then
      print("Nie ma wydarzeń po aktualnej chwili")
    else
      print("Wydarzenia z kalendarza od aktualnej chwili:")
    end
  end
  
  if #localCalendar ~= 0 then
    local sort_func = function( a,b ) return a.startTime < b.startTime end
  
    table.sort(localCalendar, sort_func )
    
    for i, e in pairs(localCalendar) do
      print("Nazwa wydarzenia: " .. e.name)
      print("Początek wydarzenia: " .. os.date("%x, %X", e.startTime)) 
      print("Koniec wydarzenia: " .. os.date("%x, %X", e.startTime) .. "\n")
    end
  end
end

addEventAndPrintInfo(kalendarz, createEventOsTime("Wykład Lua",os.time{year =2017 , month=3, day =28, hour=16, min=15},os.time{year =2017 , month=3, day =28, hour=18, min=00}))

addEventAndPrintInfo(kalendarz, createEventOsTime("Kolizja",os.time{year =2017 , month=3, day =28,hour=16,min=15},os.time{year =2017 , month=3, day =28, hour=17, min=00}))

addEventAndPrintInfo(kalendarz, createEventOsTime("Ćwiczenia Lua",os.time{year =2017 , month=3, day =28, hour=18, min=15},os.time{year =2017 , month=3, day =28, hour=20, min=00}))

addEventAndPrintInfo(kalendarz, createEventOsTime("Wykład AiSD",os.time{year =2017 , month=3, day =29, hour=10, min=15},os.time{year =2017 , month=3, day =29, hour=12, min=00}))

print("\n\n\n")
show(kalendarz, true)

print("\n\n\n")
show(kalendarz)