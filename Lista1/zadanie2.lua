t = {1, 2, 3}

local function increment (n)
  return n+1
end

function map(t, f)
  local result = {}
  for i=1, #t do
    result[i] = f(t[i])
  end
  return result
end


function printtab(tab)
  str = ''
  for i=1,#tab do
    str = str..tab[i]..' '
  end
  print (str)
end


mapped = map(t, increment)

printtab(mapped)