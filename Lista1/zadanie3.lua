function printtab(tab)
  str = ''
  for i=1,#tab do
    str = str..tab[i]..' '
  end
  print (str)
end


function primeFactors(number)
  local result = {}
  local iterator = 1
  for i=2, math.sqrt(number) do
    while number % i == 0 do
      result[iterator] = i
      iterator = iterator + 1
      number = number / i
    end
    if number == 1 then
      return result
    end
  end
  if number > 1 then
    result[iterator] = number
  end
  return result
end

number = 270814
printtab(primeFactors(number))