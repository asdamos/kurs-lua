#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <string>
#include <iostream>
#include <lua.hpp> 
#include <string>

using namespace std;

void error (lua_State *L, const char *fmt, ...) 
{
    va_list argp;
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    lua_close(L);
    exit(1);
}

// zwracamy wartość typu int zapisaną w globalnej zmiennej varname
int getglobalint (lua_State *L, const char *varname)
{
    int isnum, result;

    lua_getglobal(L, varname); // wstawiamy zmienną globalną varname na stos
    result = (int)lua_tointegerx(L, -1, &isnum); // odczytujemy ze stosu jej wartość

    if (!isnum) // sprawdzamy czy się poprawnie wczytała
        error(L, "'%s' should be a number\n", varname);
    
    lua_pop(L, 1); // usuwamy wczytaną wartość ze stosu
    return result;
}

float getglobalfloat (lua_State *L, const char *varname)
{
    int isnum;
    float result;

    lua_getglobal(L, varname); // wstawiamy zmienną globalną varname na stos
    result = (float)lua_tonumberx(L, -1, &isnum); // odczytujemy ze stosu jej wartość

    if (!isnum) // sprawdzamy czy się poprawnie wczytała
        error(L, "'%s' should be a number\n", varname);

    lua_pop(L, 1); // usuwamy wczytaną wartość ze stosu
    return result;
}

bool getglobalbool(lua_State *L, const char *varname)
{
    bool result;

    lua_getglobal(L, varname); // wstawiamy zmienną globalną varname na stos

    if (lua_isboolean(L, -1))
    {
        result = (bool)lua_toboolean(L, -1); // odczytujemy ze stosu jej wartość
    }
    else
    {
        error(L, "'%s' should be a boolean\n", varname);
    }

    lua_pop(L, 1); // usuwamy wczytaną wartość ze stosu
    return result;
}

std::string getglobalstring(lua_State *L, const char *varname)
{
    std::string result;

    lua_getglobal(L, varname); // wstawiamy zmienną globalną varname na stos

    if (lua_isstring(L, -1))
    {
        result = lua_tostring(L, -1); // odczytujemy ze stosu jej wartość
    }
    else
    {
        error(L, "'%s' should be a string\n", varname);
    }

    lua_pop(L, 1); // usuwamy wczytaną wartość ze stosu
    return result;
}

void load_cfg (lua_State *L, const char *fname)
{
    if (luaL_loadfile(L, fname)  || lua_pcall(L, 0, 0, 0))
        error(L, "cannot load config file: %s\n", lua_tostring(L, -1));
}






int main (void) 
{
    int verbose;
    std::string verbose_level;
    bool developer_debug_on;
    int window_height;
    int window_width;
    float window_ratio;
    int hp;
    int lvl;
    std::string name;
    std::string language;
    std::string server_ip;
    bool boolean;

    lua_State *L = luaL_newstate();

    load_cfg(L, "config.lua");
    std::cout <<"Plik konfiguracyjny:" << std::endl;

    verbose = getglobalint(L, "verbose");
    verbose_level = getglobalstring(L, "verbose_level");
    developer_debug_on = getglobalbool(L, "developer_debug_on");
    window_height = getglobalint(L, "window_height");
    window_ratio = getglobalfloat(L, "window_ratio");
    hp = getglobalint(L, "hp");
    lvl = getglobalint(L, "lvl");
    name = getglobalstring(L, "name");
    language = getglobalstring(L, "language");
    server_ip = getglobalstring(L, "server_ip");
    boolean = getglobalbool(L, "boolean");

    std::cout<<"Verbose: "<<verbose<<std::endl;
    std::cout<<"Verbose level: "<<verbose_level<<std::endl;
    std::cout<<"Developer debug on: "<<developer_debug_on<<std::endl;
    std::cout<<"Window height: "<<window_height<<std::endl;
    std::cout<<"Window ratio: "<<window_ratio<<std::endl;
    std::cout<<"Hp: "<<hp<<std::endl;
    std::cout<<"Lvl: "<<lvl<<std::endl;
    std::cout<<"Name: "<<name<<std::endl;
    std::cout<<"Language: "<<language<<std::endl;
    std::cout<<"Server ip: "<<server_ip<<std::endl;
    std::cout<<"Boolean: "<<boolean<<std::endl;


    lua_pushnumber(L, window_height*window_ratio);
    lua_setglobal(L, "window_width");

    cout <<std::endl<<"Zmodyfikowano plik konfiguracyjny:" << endl;

    verbose = getglobalint(L, "verbose");
    verbose_level = getglobalstring(L, "verbose_level");
    developer_debug_on = getglobalbool(L, "developer_debug_on");
    window_height = getglobalint(L, "window_height");
    window_width = getglobalint(L, "window_width");
    window_ratio = getglobalfloat(L, "window_ratio");
    hp = getglobalint(L, "hp");
    lvl = getglobalint(L, "lvl");
    name = getglobalstring(L, "name");
    language = getglobalstring(L, "language");
    server_ip = getglobalstring(L, "server_ip");
    boolean = getglobalbool(L, "boolean");

    std::cout<<"Verbose: "<<verbose<<std::endl;
    std::cout<<"Verbose level: "<<verbose_level<<std::endl;
    std::cout<<"Developer debug on: "<<developer_debug_on<<std::endl;
    std::cout<<"Window height: "<<window_height<<std::endl;
    std::cout<<"Window width: "<<window_width<<std::endl;
    std::cout<<"Window ratio: "<<window_ratio<<std::endl;
    std::cout<<"Hp: "<<hp<<std::endl;
    std::cout<<"Lvl: "<<lvl<<std::endl;
    std::cout<<"Name: "<<name<<std::endl;
    std::cout<<"Language: "<<language<<std::endl;
    std::cout<<"Server ip: "<<server_ip<<std::endl;
    std::cout<<"Boolean: "<<boolean<<std::endl;

    lua_close(L);

    return 0;
}
