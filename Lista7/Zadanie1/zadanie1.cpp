#include <stdio.h>
#include <string.h>
#include <lua.hpp> 
#include <iostream>

static void stackDump (lua_State *L) 
{
    std::cout<<"StackDump:"<<std::endl;
    int i;
    int top = lua_gettop(L);
    for (i = top; i > 0; i--) // odwiedzamy elementy od gory
    {
        std::cout<<"Indeks "<<i<<": ";
        int t = lua_type(L, i);
        switch (t)
        {
        case LUA_TSTRING: // napis
            std::cout<<"'"<<lua_tostring(L, i)<<"'"<<std::endl;
            break;

        case LUA_TBOOLEAN: // wartość logiczna
            if (lua_toboolean(L, i))
            {
                std::cout<<"true"<<std::endl;
            }
            else
            {
                std::cout<<"false"<<std::endl;
            }
            break;

        case LUA_TNUMBER: // liczby

            if (lua_isinteger(L, i)) // integer
            {
                std::cout<<lua_tointeger(L, i)<<std::endl;
            }
            else // float
            {
                std::cout<<lua_tonumber(L, i)<<std::endl;
            }
            break;

        default: // pozostałe
            std::cout<<lua_typename(L, t)<<std::endl;
            break;
        }
    }
    std::cout<<std::endl;
}


void test1()
{
    std::cout<<"Test 1"<<std::endl;
    lua_State *L = luaL_newstate();

    lua_pushboolean(L, 1);
    std::cout<<"Dodaję true"<<std::endl;
    stackDump(L);

    lua_pushnumber(L, 10);
    std::cout<<"Dodaję liczbę: 10"<<std::endl;
    stackDump(L);

    lua_pushnil(L);
    std::cout<<"Dodaję nil"<<std::endl;
    stackDump(L);

    lua_pushstring(L, "hello");
    std::cout<<"Dodaję string: \"hello\""<<std::endl;
    stackDump(L);

    lua_pushvalue(L, -4);  // true 10 nil ’hello’ true
    std::cout<<"Dodaję kopię elementu pod indeksem: -4"<<std::endl;
    stackDump(L);

    lua_replace(L, 3);     // true 10 true ’hello’
    std::cout<<"Pobieram ze stosu i wstawiam na pozycję 3"<<std::endl;
    stackDump(L);

    lua_settop(L, 6);
    std::cout<<"Ustawiam szczyt stosu na 6"<<std::endl;
    stackDump(L);

    lua_rotate(L, 3, 1);  // true 10 nil true 'hello' nil
    std::cout<<"Obracam elementy od indeksu 3 o 1 pozycję"<<std::endl;
    stackDump(L);



    lua_remove(L, -3);
    std::cout<<"Usuwam element na pozycji -3"<<std::endl;
    stackDump(L);


    lua_settop(L, -5);
    std::cout<<"Ustawiam szczyt stosu na pozycji -5"<<std::endl;
    stackDump(L);

    lua_close(L);
}

void test2()
{
    std::cout<<"Test 2"<<std::endl;
    lua_State *L = luaL_newstate();

    lua_pushnumber (L , 3.5);
    std::cout<<"Dodaję liczbę: 3.5"<<std::endl;
    stackDump(L);

    lua_pushstring (L , "hello" );
    std::cout<<"Dodaję string: \"hello\""<<std::endl;
    stackDump(L);

    lua_pushnil ( L );
    std::cout<<"Dodaję nil"<<std::endl;
    stackDump(L);

    lua_pushvalue (L , -2);
    std::cout<<"Dodaję kopię elementu pod indeksem: -2"<<std::endl;
    stackDump(L);

    lua_remove (L , 1);
    std::cout<<"Usuwam element na pozycji 1"<<std::endl;
    stackDump(L);


    lua_insert (L , -2);
    std::cout<<"Przesuwam wierzchni element na pozycję -2"<<std::endl;
    stackDump(L);

    lua_close(L);
}


int main (void) 
{
    test1();

    std::cout<<std::endl<<std::endl;

    test2();


    return 0;
}
