local function rng()
  return math.random(3)
end

local function win(mysymbol, board)
  for i=1, 3 do
    if board[i][1] == board[i][2] and board[i][1] == mysymbol and board[i][3] == " " then
      return true, i, 3
    end

    if board[i][3] == board[i][2] and board[i][3] == mysymbol and board[i][1] == " " then
      return true, i, 1
    end

    if board[i][1] == board[i][3] and board[i][1] == mysymbol and board[i][2] == " " then
      return true, i, 2
    end

    -------------------------

    if board[1][i] == board[2][i] and board[1][i] == mysymbol and board[3][i] == " " then
      return true, 3, i
    end

    if board[3][i] == board[2][i] and board[3][i] == mysymbol and board[1][i] == " " then
      return true, 1, i
    end

    if board[1][i] == board[3][i] and board[1][i] == mysymbol and board[2][i] == " " then
      return true, 2, i
    end
  end

  --------------------------------
  --skosy
  if board[1][1] == board[2][2] and board[1][1] == mysymbol and board[3][3] == " " then
    return true, 3, 3
  end

  if board[1][1] == board[3][3] and board[1][1] == mysymbol and board[2][2] == " " then
    return true, 2, 2
  end

  if board[2][2] == board[3][3] and board[2][2] == mysymbol and board[1][1] == " " then
    return true, 1, 1
  end

  ---
  if board[3][1] == board[2][2] and board[3][1] == mysymbol and board[1][3] == " " then
    return true, 1, 3
  end

  if board[3][1] == board[1][3] and board[3][1] == mysymbol and board[2][2] == " " then
    return true, 2, 2
  end

  if board[2][2] == board[1][3] and board[2][2] == mysymbol and board[3][1] == " " then
    return true, 3, 1
  end


  return false
end






AI = function(mysymbol, board)

  local res, x, y = win(mysymbol, board)

  if res == true then
    return x,y
  end
  
  --block is win with opponent symbol
  
  local opponent = "X"
  if mysymbol == "X" then opponent = "O" end
  
  res,x,y = win(opponent, board)
  
  if res == true then
    return x,y
  end
  
  if board[2][2] == " " then
    return 2,2
  end

  while true do
    local x, y = rng(), rng()
    if board[x][y] == " " then
      return x, y
    end
  end
end