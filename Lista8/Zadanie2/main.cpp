#include <iostream>
#include <cstdlib>
#include "tictactoe_lua.hpp"
int n = 1000;


int main (int argc, char *argv[])
{
    if(argc !=3)
    {
        std::cout<<"Prosze podac 2 pliki z botami"<<std::endl;
        exit(0);
    }

    std::cout<<"Testowa gra"<<std::endl;
    TicTacToe_Lua game(argv[1], argv[2]);
    game.play(1, true);

    int results[3] = {0};

    for(int i=0; i<n; ++i)
    {
        TicTacToe_Lua game1(argv[1], argv[2]);

        results[game1.play(1,false)]++;


        TicTacToe_Lua game2(argv[1], argv[2]);

        results[game2.play(2,false)]++;
    }


    std::cout<<"Rozegrano "<<n*2<<" gier"<<std::endl;
    std::cout<<"Bot w pliku: "<<argv[1]<<" wygral "<<results[1]<<" razy => "<<float(results[1])/20<<"%"<<std::endl;
    std::cout<<"Bot w pliku: "<<argv[2]<<" wygral "<<results[2]<<" razy => "<<float(results[2])/20<<"%"<<std::endl;
    std::cout<<"Boty zremisowały "<<results[0]<<" razy"<<std::endl;

    return 0;
}
