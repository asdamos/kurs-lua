#include "tictactoe_lua.hpp"


void TicTacToe_Lua::lua_loadfile(lua_State *L, const char *fname)
{
    if (luaL_loadfile(L, fname)  || lua_pcall(L, 0, 0, 0))
        error(L, "Cannot load bot in file: %s\n", lua_tostring(L, -1));
}

void TicTacToe_Lua::pushBoard(lua_State *L)
{
    lua_newtable(L);

    for (size_t i = 0; i < 3; i++)
    {
        lua_pushinteger(L, i + 1);  //tables in lua starts in index = 1
        lua_newtable(L);
        for (size_t j = 0; j < 3; j++)
        {
            lua_pushinteger(L, j + 1);  //tables in lua starts in index = 1
            lua_pushstring(L, board[i][j].c_str());
            lua_settable(L, -3);
        }

        lua_settable(L, -3);
    }
}

TicTacToe_Lua::TicTacToe_Lua(std::string player1, std::string player2)
{
    p1 = luaL_newstate();
    luaL_openlibs(p1);
    lua_loadfile(p1, player1.c_str());

    p2 = luaL_newstate();
    luaL_openlibs(p2);
    lua_loadfile(p2, player2.c_str());

    for(int i=0; i<3; ++i)
    {
        for(int j=0; j<3; ++j)
        {
            board[i][j] = " ";
        }
    }
}

TicTacToe_Lua::~TicTacToe_Lua()
{
    lua_close(p1);
    lua_close(p2);
}

void TicTacToe_Lua::make_move(int player)
{
    lua_State *current_player;
    std::string playerCharacter;


    if(player == 1)
    {
        current_player = p1;
        playerCharacter = "X";
    }
    else
    {
        current_player = p2;
        playerCharacter = "O";
    }

    lua_getglobal(current_player, "AI");
    lua_pushstring(current_player, playerCharacter.c_str());
    pushBoard(current_player);

    if (lua_pcall(current_player, 2, 2, 0) != LUA_OK)
        error(current_player, "Could not run function AI");

    int isnum1, isnum2;
    int x = lua_tonumberx(current_player, -2, &isnum1);
    int y = lua_tonumberx(current_player, -1, &isnum2);
    if (!isnum1 || !isnum2)
        error(current_player, "AI did not return two ints");

    if( board[x-1][y-1] != " ")
    {
        error(current_player, "AI placed character at wrong field");
    }
    else
    {
        board[x - 1][y - 1] = playerCharacter;

    }
    lua_pop(current_player, 2);
}

void TicTacToe_Lua::draw_board()
{
    for(int i=0; i<3; ++i)
    {
        for(int j=0; j<3; ++j)
        {
            if(board[i][j] == " ")
            {
                std::cout<<" _ ";
            }
            else
            {
                std::cout<<" "<<board[i][j]<<" ";
            }
        }
        std::cout<<std::endl;
    }
}

bool TicTacToe_Lua::game_over()
{
    for(int i=0; i<3; i++)
    {
        if(board[i][0] == board[i][1] && board[i][1]==board[i][2] && board[i][0]!=" ")
        {
            return true;
        }
    }

    for(int i=0; i<3; i++)
    {
        if(board[0][i] == board[1][i] && board[1][i]==board[2][i] && board[0][i]!=" ")
        {
            return true;
        }
    }

    if(board[0][0] == board[1][1] && board[1][1]==board[2][2] && board[0][0]!=" ")
    {
        return true;
    }

    if(board[2][0] == board[1][1] && board[1][1]==board[0][2] && board[2][0]!=" ")
    {
        return true;
    }

    if(moves_number == 9)
    {
        return true;
    }

    return false;

}

bool TicTacToe_Lua::character_won(std::string c)
{
    for(int i=0; i<3; i++)
    {
        if(board[i][0] == board[i][1] && board[i][1]==board[i][2] && board[i][0]==c)
        {
            return true;
        }
    }

    for(int i=0; i<3; i++)
    {
        if(board[0][i] == board[1][i] && board[1][i]==board[2][i] && board[0][i]==c)
        {
            return true;
        }
    }

    if(board[0][0] == board[1][1] && board[1][1]==board[2][2] && board[0][0]==c)
    {
        return true;
    }

    if(board[2][0] == board[1][1] && board[1][1]==board[0][2] && board[2][0]==c)
    {
        return true;
    }

    return false;
}


int TicTacToe_Lua::play(int starting_player, bool show_info)
{
    int current_player = starting_player;
    while(!game_over())
    {
        make_move(current_player);
        moves_number++;
        if(show_info)
        {
            std::cout<<"Gracz numer "<<current_player<<" wykonal ruch"<<std::endl;
            draw_board();
        }

        if(current_player == 1) current_player = 2;
        else current_player = 1;
    }

    int res;
    if(character_won("X"))
    {
        res = 1;
    }
    else if(character_won("O"))
    {
        res = 2;
    }
    else
    {
        res = 0;
    }

    if(show_info)
    {
        if(res != 0)
        {
            std::cout<<"Wygral gracz numer "<<res<<std::endl;

        }
        else
        {
            std::cout<<"REMIS!"<<std::endl;
        }
    }
    return res;
}
