#ifndef TICTACTOE_LUA_H
#define TICTACTOE_LUA_H

#include <lua.hpp>
#include <string>
#include <iostream>
#include <cstdlib>

class TicTacToe_Lua
{
private:
    std::string board[3][3];

    int moves_number = 0;

    lua_State *p1, *p2;

    void lua_loadfile(lua_State *L, const char *fname);
    void pushBoard(lua_State *L);

    void make_move(int player);
    void draw_board();
    bool game_over();
    bool character_won(std::string c);

    void error (lua_State *L, const char *fmt, ...)
    {
        va_list argp;
        va_start(argp, fmt);
        vfprintf(stderr, fmt, argp);
        va_end(argp);
        lua_close(L);
        exit(1);
    }

    static void stackDump (lua_State *L)
    {
        std::cout<<"StackDump:"<<std::endl;
        int i;
        int top = lua_gettop(L);
        for (i = top; i > 0; i--) // odwiedzamy elementy od gory
        {
            std::cout<<"Indeks "<<i<<": ";
            int t = lua_type(L, i);
            switch (t)
            {
            case LUA_TSTRING: // napis
                std::cout<<"'"<<lua_tostring(L, i)<<"'"<<std::endl;
                break;

            case LUA_TBOOLEAN: // wartość logiczna
                if (lua_toboolean(L, i))
                {
                    std::cout<<"true"<<std::endl;
                }
                else
                {
                    std::cout<<"false"<<std::endl;
                }
                break;

            case LUA_TNUMBER: // liczby

                if (lua_isinteger(L, i)) // integer
                {
                    std::cout<<lua_tointeger(L, i)<<std::endl;
                }
                else // float
                {
                    std::cout<<lua_tonumber(L, i)<<std::endl;
                }
                break;

            default: // pozostałe
                std::cout<<lua_typename(L, t)<<std::endl;
                break;
            }
        }
        std::cout<<std::endl;
    }



public:
    TicTacToe_Lua(std::string player1, std::string player2);
    ~TicTacToe_Lua();
    int play(int starting_player, bool show_info);
};


#endif
