function zip(...)
  local tab = table.pack(...)
  local min_length = #tab[1]

  for i, t in ipairs(tab) do
    if min_length > #t then
      min_length = #t
    end
  end

  return function(state)
    if state[2] > state[3] then
      return nil
    end
    local res = {}

    for i, t in ipairs(state[1]) do
      res[#res + 1] = t[state[2]]
    end

    state[2] = state[2] + 1

    return table.unpack(res)

  end, {tab, 1, min_length}

end


for x , y in zip ({ 'a' , 'b' , 'c' , 'd'} , {40 , 50 , 60}) do
  print (x , y )
end