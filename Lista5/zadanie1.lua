function chain(...)
  local tab = table.pack(...)

  return function(state)
    local iter = 0
    for i, t in ipairs(state[1]) do

      for j, k in ipairs(t) do

        if iter == state[2] then
          state[2] = state[2] + 1
          return k
        end

        iter = iter + 1

      end
    end

    return nil

  end, {tab, 0}

end

for x in chain ({ 'a' , 'b' , 'c'} , {40 , 50} , {} , {6 , 7}) do
  print ( x )
end
