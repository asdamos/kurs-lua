local Frac = {}
Frac.mt = {}

local function gcd(m, n)
  m = math.abs(m)
  n = math.abs(n)

  while m ~= 0 do
    m, n = (n % m), m;
  end

  return n;
end

function Frac.new(num, den)

  numerator = num
  denominator = den

  if denominator < 0 then
    numerator = numerator * -1
    denominator = denominator * -1
  end

  local div = gcd(numerator, denominator)

  numerator = numerator / div
  denominator = denominator / div
  
  frac = {numerator = numerator, denominator = denominator}
  setmetatable(frac, Frac.mt)
  return frac
end

function Frac.add(frac1, frac2)

  local numerator = frac1.numerator * frac2.denominator + frac2.numerator * frac1.denominator
  local denominator = frac1.denominator * frac2.denominator

  return Frac.new(numerator, denominator)
end

function Frac.sub(frac1, frac2)

  local numerator = frac1.numerator * frac2.denominator - frac2.numerator * frac1.denominator
  local denominator = frac1.denominator * frac2.denominator

  return Frac.new(numerator, denominator)
end

function Frac.mul(frac1, frac2)

  local numerator = frac1.numerator * frac2.numerator
  local denominator = frac1.denominator * frac2.denominator

  return Frac.new(numerator, denominator)
end

function Frac.div(frac1, frac2)

  local numerator = frac1.numerator * frac2.denominator
  local denominator = frac1.denominator * frac2.numerator

  return Frac.new(numerator, denominator)
end

function Frac.unminus(frac)

  frac.numerator = frac.numerator * -1
  return frac
end

function Frac.equal(frac1, frac2)
    return frac1.numerator == frac2.numerator and frac1.denominator == frac2.denominator
end

function Frac.lessThan(frac1, frac2)
  return frac1.numerator * frac2.denominator < frac2.numerator * frac1.denominator
end

function Frac.lessEqual(frac1, frac2)
  return frac1.numerator * frac2.denominator <= frac2.numerator * frac1.denominator
end

function Frac.tostring(frac)
  return tostring(frac.numerator) .. '/' .. tostring(frac.denominator)
end

function Frac.concat(var1, var2)
  
  if type(var1) == "string" then
    return var1 .. Frac.tostring(var2)
  end
  
  return Frac.tostring(var1) .. var2
  
end

function Frac.toFloat(frac)
  return frac.numerator / frac.denominator
end

function Frac.pow(frac, exponent)
  
  if type(exponent) ~= "number" then
    error("Wykładnik nie jest liczbą całkowitą")
  end

  if exponent % 1 ~= 0 then
    error("Wykładnik nie jest liczbą całkowitą")
  end
  
  return Frac.new(math.pow(frac.numerator, exponent), math.pow(frac.denominator, exponent))

end





Frac.mt.__add = Frac.add
Frac.mt.__sub = Frac.sub
Frac.mt.__mul = Frac.mul
Frac.mt.__div = Frac.div
Frac.mt.__unm = Frac.unminus

Frac.mt.__eq = Frac.equal
Frac.mt.__lt = Frac.lessThan
Frac.mt.__le = Frac.lessEqual

Frac.mt.__tostring = Frac.tostring
Frac.mt.__concat = Frac.concat

Frac.mt.__pow = Frac.pow

return Frac
